import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './views/home/home.component';
import { RoomComponent } from './views/room/room.component';
import { BedComponent } from './views/bed/bed.component';
import { DataComponent } from './views/bed/data/data.component';
import { TodosComponent } from './views/bed/todos/todos.component';
import { NotesComponent } from './views/bed/notes/notes.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RoomComponent,
    BedComponent,
    DataComponent,
    TodosComponent,
    NotesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
