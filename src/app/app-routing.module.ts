import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './views/home/home.component';
import { RoomComponent } from './views/room/room.component';
import { BedComponent } from './views/bed/bed.component';
import { DataComponent } from './views/bed/data/data.component';
import { TodosComponent } from './views/bed/todos/todos.component';
import { NotesComponent } from './views/bed/notes/notes.component';


const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'room/:roomId',
    component: RoomComponent
  },
  {
    path: 'bed/:bedId',
    component: BedComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'data'
      },
      {
        path: 'data',
        component: DataComponent
      },
      {
        path: 'todos',
        component: TodosComponent
      },
      {
        path: 'notes',
        component: NotesComponent
      }
    ]
  },
  {
    path: '**',
    redirectTo: '/',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
