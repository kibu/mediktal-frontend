# MeDiktál Frontend

## Wireframe
https://www.figma.com/proto/Y1vshWJvmZVakQRNaPQY92/MeDiktal?node-id=61%3A1235

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
